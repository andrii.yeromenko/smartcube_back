let users:userProps[]  = [];

export type userProps = {
    id: string,
    name: string
}

export const addUser = (props:userProps) => {
    const nameNormalized = props.name.trim().toLocaleLowerCase();
    const existingUser = users.find(u => u.name === nameNormalized);

    if(existingUser){
       throw new Error("This username is taken");
    }

    const user:userProps = {
        id: props.id,
        name: props.name
    };

    users.push(user);
    return user;
};

export const removeUser = (id: string) => {
    const user = users.find(u => u.id === id);
    if(!user)
    {
        throw new Error(`User with id ${id} not fond`);
    }

    users = users.filter(elem => elem !== user);
};

export const getUser = (id: string) => {
   const user =  users.find(u => u.id === id);
   if(!user)
   {
       throw new Error(`User with id ${id} not fond`);
   }
    return user;
};